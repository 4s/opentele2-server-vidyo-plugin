package org.opentele.server.video

import org.springframework.security.core.AuthenticationException

public class UnknownVideoCommunicationException extends AuthenticationException {
    public UnknownVideoCommunicationException(Throwable cause) {
        super(cause)
    }
}
