package org.opentele.server.video

import org.springframework.beans.factory.annotation.Value

class VideoConferenceService {

    @Value('${video.client.version:0.0}')
    Integer videoClientVersion

    @Value('${video.serviceURL:null}')
    String videoServiceURL

    @Value('${video.portalURL:null}')
    String videoPortalURL

    boolean userIsAlreadyPresentInOwnRoom(String userName, String password) {
        def soapClient = createVidyoSoapClient(userName, password)

        def userEntityId = entityIdOfUser(soapClient)
        participantsInConference(soapClient, userEntityId).any {
            def (entityId, _) = it
            entityId == userEntityId
        }
    }

    String initializeCallAndCreateClientParameters(String userName, String password) {
        def soapClient = createVidyoSoapClient(userName, password)

        def userEntityId = entityIdOfUser(soapClient)
        doEndConference(soapClient, userEntityId)
        changeKeyForUsersRoom(soapClient, userEntityId)
        def clientParameters = createClientParameters(soapClient, userName)

        clientParameters
    }

    String linkEndpoint(String userName, String password, String endpointId) {
        def soapClient = createVidyoSoapClient(userName, password)

        linkEndpointAndGetRoomKey(soapClient, endpointId)
    }

    void joinConference(String userName, String password) {
        def soapClient = createVidyoSoapClient(userName, password)

        def userEntityId = entityIdOfUser(soapClient)
        doJoinConference(soapClient, userEntityId)
    }

    void endConference(String userName, String password) {
        def soapClient = createVidyoSoapClient(userName, password)

        def userEntityId = entityIdOfUser(soapClient)
        doEndConference(soapClient, userEntityId)
        logOut(soapClient)
    }

    private void doEndConference(VidyoSoapClient soapClient, conferenceId) {
        participantsInConference(soapClient, conferenceId).each {
            def (_, participantId) = it
            leaveConference(soapClient, conferenceId, participantId)
        }
    }

    private void logOut(VidyoSoapClient soapClient) {
        def logOutResponse = soapClient.send(SOAPAction: 'logOut') {
            body {
                LogOutRequest(xmlns: 'http://portal.vidyo.com/user/v1_1')
            }
        }
        def result = logOutResponse.LogOutResponse.OK.text()
        log.info "Log out response: $result"
    }

    private entityIdOfUser(VidyoSoapClient soapClient) {
        def myAccountResponse = soapClient.send(SOAPAction: 'MyAccount') {
            body {
                MyAccountRequest(xmlns: 'http://portal.vidyo.com/user/v1_1')
            }
        }
        def result = myAccountResponse.MyAccountResponse.Entity.entityID.text()
        result
    }

    private List<String> participantsInConference(VidyoSoapClient soapClient, conferenceId) {
        def participantsResponse = soapClient.send(SOAPAction: 'getParticipants') {
            body {
                GetParticipants(xmlns: 'http://portal.vidyo.com/user/v1_1') {
                    conferenceID(conferenceId)
                }
            }
        }
        participantsResponse.GetParticipantsResponse.Entity.collect {
            [it.entityID.text(), it.participantID.text()]
        }
    }

    private leaveConference(VidyoSoapClient soapClient, conferenceId, participantId) {
        soapClient.send(SOAPAction: 'leaveConference') {
            body {
                LeaveConference(xmlns: 'http://portal.vidyo.com/user/v1_1') {
                    conferenceID(conferenceId)
                    participantID(participantId)
                }
            }
        }
    }

    private changeKeyForUsersRoom(VidyoSoapClient soapClient, entityId) {
        soapClient.send(SOAPAction: 'CreateRoomURL') {
            body {
                CreateRoomURLRequest(xmlns: 'http://portal.vidyo.com/user/v1_1') {
                    roomID(entityId)
                }
            }
        }
    }

    private createClientParameters(VidyoSoapClient soapClient, videoUser) {
        def logInResponse = soapClient.send(SOAPAction: 'logIn') {
            body {
                LogInRequest(xmlns: 'http://portal.vidyo.com/user/v1_1')
            }
        }

        def portalAccessKey = logInResponse.LogInResponse.pak.text()
        def vmAddress = logInResponse.LogInResponse.vmaddress.text()
        def locationTag = logInResponse.LogInResponse.loctag.text()
        def proxyAddress = logInResponse.LogInResponse.proxyaddress.text()

        def portal = ""
        switch (videoClientVersion) {
            case 2:
                portal = videoServiceURL
                break;

            case 3:
                portal = videoPortalURL
                break;

            default:
                log.error "Unknown video client version $videoClientVersion"
                break;
        }

        log.info "Client version: $videoClientVersion"
        log.info "Portal access key: $portalAccessKey"
        log.info "VM Address: $vmAddress"
        log.info "Location tag: $locationTag"
        log.info "Portal: $portal"

        "vm=${vmAddress}&un=${videoUser}&pak=${portalAccessKey}&portal=${portal}&proxy=${proxyAddress}"
    }

    private linkEndpointAndGetRoomKey(VidyoSoapClient soapClient, endpointId) {
        def linkEndpointResponse = soapClient.send(SOAPAction: 'linkEndpoint') {
            body {
                LinkEndpointRequest(xmlns: 'http://portal.vidyo.com/user/v1_1') {
                    EID(endpointId)
                }
            }
        }

        def roomUrl = linkEndpointResponse.LinkEndpointResponse.Entity.RoomMode.roomURL.text()
        def roomKey = parseRoomKey(roomUrl)
        log.info "Room key: $roomKey"

        roomKey
    }

    private doJoinConference(VidyoSoapClient soapClient, entityId) {
        soapClient.send(SOAPAction: 'JoinConference') {
            body {
                JoinConferenceRequest(xmlns: 'http://portal.vidyo.com/user/v1_1') {
                    conferenceID(entityId)
                }
            }
        }
    }

    private parseRoomKey(String roomUrl) {
        roomUrl.split('key=').last()
    }

    private VidyoSoapClient createVidyoSoapClient(userName, password) {
        new VidyoSoapClient(userName, password, videoServiceURL)
    }
}
