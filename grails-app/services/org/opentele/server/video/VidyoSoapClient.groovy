package org.opentele.server.video

import groovy.util.logging.Log4j
import org.springframework.security.authentication.BadCredentialsException
import wslite.http.auth.HTTPBasicAuthorization
import wslite.soap.SOAPClient
import wslite.soap.SOAPClientException
import wslite.soap.SOAPFaultException
import wslite.soap.SOAPResponse

@Log4j
class VidyoSoapClient {
    private final SOAPClient client

    VidyoSoapClient(userName, password, serviceUrl) {
        client = new SOAPClient(serviceUrl)
        client.authorization = new HTTPBasicAuthorization(userName, password)
    }

    SOAPResponse send(Map requestParams=[:], Closure content) {
        try {
            client.send(requestParams, content)
        } catch (SOAPFaultException e) {
            log.error("SOAP fault message: ${e.message}")
            log.error("SOAP fault text: ${e.text}")
            log.error("SOAP fault status code: ${e.httpResponse.statusCode}")
            log.error("SOAP fault detail text: ${e.fault.detail.text()}")

            if (e.fault.detail.text() == 'Failed to Leave Conference') {
                // Seems harmless, but very annoying
                log.error('Failed to leave conference. Ignoring.')
            } else {
                throw new UnknownVideoCommunicationException(e);
            }
        } catch (SOAPClientException e) {
            if (e?.response?.statusCode == 401) {
                throw new BadCredentialsException('Credentials not valid for Vidyo', e);
            }
            throw new UnknownVideoCommunicationException(e);
        }
    }
}
