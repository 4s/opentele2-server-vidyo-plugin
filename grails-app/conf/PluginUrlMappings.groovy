class PluginUrlMappings {

	static mappings = {
        '/videoResource/VidyoDesktopClientStarter.jar'{
            controller = 'videoResource'
            action = 'vidyoDesktopClientStarterJar'
        }

		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
