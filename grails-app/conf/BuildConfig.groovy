grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

// Repository to push plugin to
grails.project.repos.oth.url = "https://repository.oth.io/nexus/content/repositories/opentele-server-plugins/"
grails.project.repos.oth.username = "oth-developer"
grails.project.repos.oth.password = "<insert-password>"
grails.project.repos.default = "oth"

// Repository to fetch plugins from
grails.project.ivy.authentication = {
    repositories {
        mavenRepo "https://repository.oth.io/nexus/content/repositories/opentele-server-plugins/"

    }

    credentials {
        username = System.getProperty('oth.repo.username')
        password = System.getProperty('oth.repo.password')
    }
}

grails.project.dependency.resolution = {

    inherits("global") { }

    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'

    repositories {
        grailsCentral()
        mavenCentral()
        mavenRepo "https://repository.oth.io/nexus/content/repositories/build-dependencies/"
    }

    dependencies {
        compile "net.sf.ehcache:ehcache-core:2.6.9"
        runtime 'com.github.groovy-wslite:groovy-wslite:0.7.2'
    }

    plugins {
        compile ':spring-security-core:2.0-RC4'
        build(":tomcat:7.0.54",
              ":release:3.0.1",
              ":rest-client-builder:2.0.3") {
            export = false
        }
    }
}
