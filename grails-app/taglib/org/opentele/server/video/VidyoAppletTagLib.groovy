package org.opentele.server.video

class VidyoAppletTagLib {
    static namespace = "video"

    def applet = { attrs ->
        def clientParameters = attrs.clientParameters
        def callback = attrs.callback
        def clientNotRunningCallback = attrs.clientNotRunningCallback
        if (!clientParameters || !callback || !clientNotRunningCallback) {
            throwTagError("Missing clientParameters, callback, or clientNotRunningCallback")
        }

        def jarPath = g.createLink(controller: 'videoResource', action: 'vidyoDesktopClientStarterJar').toString()

        out << """
<applet mayscript="true" code="VidyoDesktopClientStarter" archive="${jarPath}?timestamp=${System.currentTimeMillis()}" width="30" height="30" id="vidyoApplet" name="Vidyo Desktop Client Starter">
    <param name="permissions" value="all-permissions"></param>
    <param name="MAYSCRIPT" value="true"></param>
    <param name="clientParameters" value="${clientParameters}"></param>
    <param name="callback" value="${callback}"></param>
    <param name="clientNotRunningCallback" value="${clientNotRunningCallback}"></param>
</applet>
"""
    }

}
