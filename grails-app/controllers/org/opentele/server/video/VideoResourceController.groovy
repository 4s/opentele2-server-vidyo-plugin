package org.opentele.server.video

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader

@Secured(['permitAll'])
class VideoResourceController implements ResourceLoaderAware {
    def grailsApplication
    ResourceLoader resourceLoader

    def vidyoDesktopClientStarterJar() {
        def configuredFilePath = grailsApplication.config.video?.applet?.filePath
        def jarFile = configuredFilePath ? new File(configuredFilePath) : resourceLoader.getResource("${pluginContextPath}/applets/VidyoDesktopClientStarter.jar").file

        response.contentType = 'application/java-vm'
        response.setHeader 'Content-disposition', "attachment; filename=\"VidyoDesktopClientStarter.jar\""
        response.outputStream << jarFile.getBytes()
        response.outputStream.flush()
    }
}
