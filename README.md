Vidyo plug-in
=============
Gives the Vidyo-specific functionality required by the opentele-server project.

Starting the VidyoDesktop client requires a Java applet, which is checked into this repository as

        web-app/applets/VidyoDesktopClientStarter.jar

This is built by checking out another project: opentele-server-vidyo-applet. Please consult the README.md
file of that project for more details.
